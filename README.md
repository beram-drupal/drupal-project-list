# List of Drupal projects

List of useful Drupal projects grouped by their use.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Modules](#modules)
    - [Administration](#administration)
    - [Analytics](#analytics)
    - [Block](#block)
    - [Configuration](#configuration)
    - [Contact Form](#contact-form)
    - [Content authoring](#content-authoring)
    - [Cron enhancement](#cron-enhancement)
    - [Development](#development)
    - [Display](#display)
    - [Editor](#editor)
    - [Entity](#entity)
    - [Field](#field)
        - [Field type](#field-type)
        - [Widget](#widget)
        - [Field display](#field-display)
    - [Internationalization](#internationalization)
    - [Mail](#mail)
    - [Media management](#media-management)
    - [Menu](#menu)
    - [Panels](#panels)
    - [Performance](#performance)
    - [Permission](#permission)
    - [Role](#role)
    - [Search](#search)
    - [Security](#security)
    - [SEO](#seo)
    - [Slideshow](#slideshow)
    - [Social](#social)
    - [Spam control](#spam-control)
    - [User](#user)
    - [User interface](#user-interface)
    - [Workflow](#workflow)
- [Themes](#themes)
    - [Administration](#administration-1)
    - [Base](#base)
    - [Others](#others)
- [Distributions](#distributions)
    - [Configurations](#configurations)
    - [Demo](#demo)
    - [Starter kit](#starter-kit)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Modules

## Administration

Admin Toolbar: https://www.drupal.org/project/admin_toolbar

Image styles mapping: https://www.drupal.org/project/image_styles_mapping

Masquerade: https://www.drupal.org/project/masquerade

Monitoring: https://www.drupal.org/project/monitoring

Scheduled maintenance: https://www.drupal.org/project/scheduled_maintenance

Simplify: https://www.drupal.org/project/simplify

Toolbar Menu: https://www.drupal.org/project/toolbar_menu

## Analytics

Google Analytics: https://www.drupal.org/project/google_analytics

Piwik Web Analytics: https://www.drupal.org/project/piwik

## Block

Block class: https://www.drupal.org/project/block_class

Block Visibility Groups: https://www.drupal.org/project/block_visibility_groups

## Configuration

Configuration Rewrite: https://www.drupal.org/project/config_rewrite

Config overrride: https://www.drupal.org/project/config_override

Configuration Split: https://www.drupal.org/project/config_split

Config Role Split: https://www.drupal.org/project/config_role_split

Config Ignore: https://www.drupal.org/project/config_ignore

Configuration Update Manager: https://www.drupal.org/project/config_update

Features: https://www.drupal.org/project/features

## Contact Form

Contact Storage: https://www.drupal.org/project/contact_storage

Contact Form Summary: https://www.drupal.org/project/contact_form_summary

## Content authoring

Bricks: https://www.drupal.org/project/bricks

Paragraphs: https://www.drupal.org/project/paragraphs

Paragraphs Previewer: https://www.drupal.org/project/paragraphs_previewer

Paragraphs Edit: https://www.drupal.org/project/paragraphs_edit

Page Manager: https://www.drupal.org/project/page_manager

Panelizer: https://www.drupal.org/project/panelizer

## Cron enhancement

Ultimate Cron: https://www.drupal.org/project/ultimate_cron

## Development

Coder: https://www.drupal.org/project/coder

Composer Security Checker: https://www.drupal.org/project/composer_security_checker

Configuration inspector: https://www.drupal.org/project/config_inspector

Devel: https://www.drupal.org/project/devel

Devel accessibility: https://www.drupal.org/project/devel_a11y

Diff: https://www.drupal.org/project/diff

Dummy image: https://www.drupal.org/project/dummyimage

Entity Relationship Diagrams: https://www.drupal.org/project/erd

Memory profiler: https://www.drupal.org/project/memory_profiler

Search kint: https://www.drupal.org/project/search_kint

Stage File Proxy: https://www.drupal.org/project/stage_file_proxy

Style Guide: https://www.drupal.org/project/styleguide

Twig tweak: https://www.drupal.org/project/twig_tweak

UI Patterns: https://www.drupal.org/project/ui_patterns

Views Templates: https://www.drupal.org/project/views_templates

## Display

@font-your-face: https://www.drupal.org/project/fontyourface

Bootstrap related modules: https://www.drupal.org/node/2011034

Context: https://www.drupal.org/project/context

Display suite: https://www.drupal.org/project/ds

Domain Theme Switch: https://www.drupal.org/project/domain_theme_switch

Fences: https://www.drupal.org/project/fences

Font Awesome Icons: https://www.drupal.org/project/fontawesome

Form mode manager: https://www.drupal.org/project/form_mode_manager

Quick tabs: https://www.drupal.org/project/quicktabs

Switch Page Theme: https://www.drupal.org/project/switch_page_theme

ThemeKey: https://www.drupal.org/project/themekey

View Mode Page: https://www.drupal.org/project/view_mode_page

## Editor

Linkit: https://www.drupal.org/project/linkit

D8 Editor File upload: https://www.drupal.org/project/editor_file

D8 Editor Advanced link: https://www.drupal.org/project/editor_advanced_link

Pathologic: https://www.drupal.org/project/pathologic

## Entity

Entity Class Formatter: https://www.drupal.org/project/entity_class_formatter

## Field

Conditional fields: https://www.drupal.org/project/conditional_fields

Display fields: https://www.drupal.org/project/display_fields

Field default token: https://www.drupal.org/project/field_default_token

Field group: https://www.drupal.org/project/field_group

Field Image Style: https://www.drupal.org/project/field_image_style

Maxlength: https://www.drupal.org/project/maxlength

### Field type

Address: https://www.drupal.org/project/address

Dynamic Entity Reference: https://www.drupal.org/project/dynamic_entity_reference

Entity reference integrity: https://www.drupal.org/project/entity_reference_integrity

Machine name: https://www.drupal.org/project/machine_name

Name Field: https://www.drupal.org/project/name

Range: https://www.drupal.org/project/range

### Widget

Better Formats: https://www.drupal.org/project/better_formats

Chosen: https://www.drupal.org/project/chosen

Improved multi select: https://www.drupal.org/project/improved_multi_select

Inline entity form: https://www.drupal.org/project/inline_entity_form

jQuery UI MultiSelect Widget: https://www.drupal.org/project/jquery_ui_multiselect_widget

Multiple selects: https://www.drupal.org/project/multiple_selects

Multiselect: https://www.drupal.org/project/multiselect

### Field display

Compact date/time range formatter: https://www.drupal.org/project/daterange_compact

Extra Field: https://www.drupal.org/project/extra_field

Field multiple limit: https://www.drupal.org/project/field_multiple_limit

Obfuscate Email: https://www.drupal.org/project/obfuscate_email

Smart trim: https://www.drupal.org/project/smart_trim

## Internationalization

String Overrides: https://www.drupal.org/project/stringoverrides

Translation Management Tool: https://www.drupal.org/project/tmgmt

## Mail

Mail control: https://www.drupal.org/project/mailcontrol

Mail Editor: https://www.drupal.org/project/mail_edit

Mail system: https://www.drupal.org/project/mailsystem

Swiftmailer: https://www.drupal.org/project/swiftmailer

## Media management

Drupal 8 Media Guide: https://drupal-media.gitbooks.io/drupal8-guide/content

Remote Stream Wrapper: https://www.drupal.org/project/remote_stream_wrapper

## Menu

Colossal Menu: https://www.drupal.org/project/colossal_menu

Drupal 8 MegaMenu: https://www.drupal.org/project/we_megamenu

Font Awesome Menu Icons: https://www.drupal.org/project/fontawesome_menu_icons

Menu admin per menu: https://www.drupal.org/project/menu_admin_per_menu

Menu attributes: https://www.drupal.org/project/menu_attributes

Menu block: https://www.drupal.org/project/menu_block

Menu Firstchild: https://www.drupal.org/project/menu_firstchild

Menu Item Extras: https://www.drupal.org/project/menu_item_extras

Menu token: https://www.drupal.org/project/menu_token

Responsive and off-canvas menu: https://www.drupal.org/project/responsive_menu

Responsive Menus: https://www.drupal.org/project/responsive_menus

Simple Mega Menu: https://www.drupal.org/project/simple_megamenu

## Panels

Panels: https://www.drupal.org/project/panels

## Performance

http://redcrackle.com/blog/performance/drupal-performance-optimization-checklist

Advanced CSS/JS Aggregation: https://www.drupal.org/project/advagg

DB Maintenance: https://www.drupal.org/project/db_maintenance

HTTP/2 Server Push: https://www.drupal.org/project/http2_server_push

ImageAPI Optimize: https://www.drupal.org/project/imageapi_optimize

Fast 404: https://www.drupal.org/project/fast_404

Memcache Storage: https://www.drupal.org/project/memcache_storage

OptimizeDB: https://www.drupal.org/project/optimizedb

Progressive Web App: https://www.drupal.org/project/pwa

Purge: https://www.drupal.org/project/purge

Redis: https://www.drupal.org/project/redis

RefreshLess: https://www.drupal.org/project/refreshless

Sessionless BigPipe: https://www.drupal.org/project/big_pipe_sessionless

Views Custom Cache Tags: https://www.drupal.org/project/views_custom_cache_tag

## Permission

Field permissions: https://www.drupal.org/project/field_permissions

View unpublished: https://www.drupal.org/project/view_unpublished

Contact Form Permissions: https://www.drupal.org/project/contact_form_permissions

## Role

Administer Users by Role: https://www.drupal.org/project/administerusersbyrole

Roleassign: https://www.drupal.org/project/roleassign

User Personas: https://www.drupal.org/project/personas

## Search

Elasticsearch Connector: https://www.drupal.org/project/elasticsearch_connector

Elasticsearch Helper: https://www.drupal.org/project/elasticsearch_helper

Facets: https://www.drupal.org/project/facets

Search API: https://www.drupal.org/project/search_api

Search API AZ Glossary: https://www.drupal.org/project/search_api_glossary

Search API attachments: https://www.drupal.org/project/search_api_attachments

Search API Elasticsearch: https://www.drupal.org/project/search_api_elasticsearch

Search API saved searches: https://www.drupal.org/project/search_api_saved_searches

Search API Solr: https://www.drupal.org/project/search_api_solr

Search API View Modes: https://www.drupal.org/project/search_api_view_modes

## Security

Automated Logout: https://www.drupal.org/project/autologout

ClamAV: https://www.drupal.org/project/clamav

Clear saved password field: https://www.drupal.org/project/clear_password_field

Diskfree: https://www.drupal.org/project/diskfree

Encrypt: https://www.drupal.org/project/encrypt

Flood control: https://www.drupal.org/project/flood_control

Force Password Change: https://www.drupal.org/project/force_password_change

Hacked!: https://www.drupal.org/project/hacked

Login History: https://www.drupal.org/project/login_history

Login Security: https://www.drupal.org/project/login_security

Logging and alerts: https://www.drupal.org/project/logging_alerts

Mass Password Reset: https://www.drupal.org/project/mass_pwreset

No Autocomplete: https://www.drupal.org/project/no_autocomplete

Password policy: https://www.drupal.org/project/password_policy

Real Name: https://www.drupal.org/project/realname

Rename Admin Paths: https://www.drupal.org/project/rename_admin_paths

Secure Login: https://www.drupal.org/project/securelogin

Security Kit: https://www.drupal.org/project/seckit

Session Limit: https://www.drupal.org/project/session_limit

Site Audit: https://www.drupal.org/project/site_audit

User registration password: https://www.drupal.org/project/user_registrationpassword

Username Enumeration Prevention: https://www.drupal.org/project/username_enumeration_prevention

## SEO

Link checker: https://www.drupal.org/project/linkchecker

Metatag: https://www.drupal.org/project/metatag

Pathauto: https://www.drupal.org/project/pathauto

Redirect: https://www.drupal.org/project/redirect

RobotsTxt: https://www.drupal.org/project/robotstxt

Site verification: https://www.drupal.org/project/site_verify

Sub pathauto: https://www.drupal.org/project/subpathauto

W3C Validator: https://www.drupal.org/project/w3c_validator

XML sitemap: https://www.drupal.org/project/xmlsitemap

## Slideshow

Slick Carousel: https://www.drupal.org/project/slick

Views Slideshow: https://www.drupal.org/project/views_slideshow

## Social

ShareThis: https://www.drupal.org/project/sharethis

## Spam control

Captcha: https://www.drupal.org/project/captcha

Honeypot: https://www.drupal.org/project/honeypot

reCaptcha: https://www.drupal.org/project/recaptcha

## User

Persistent Login: https://www.drupal.org/project/persistent_login

Redirect 403 to User Login: https://www.drupal.org/project/r4032login

Restrict password change: https://www.drupal.org/project/restrict_password_change

User Expire: https://www.drupal.org/project/user_expire

User protect: https://www.drupal.org/project/userprotect

## User interface

Back To Top: https://www.drupal.org/project/back_to_top

Field help helper: https://www.drupal.org/project/field_help_helper

Responsive Theme Preview: https://www.drupal.org/project/responsive_preview

Scroll to top: https://www.drupal.org/project/scroll_to_top

## Workflow

Content Moderation Notifications: https://www.drupal.org/project/content_moderation_notifications

Maestro: https://www.drupal.org/project/maestro

Scheduled Updates: https://www.drupal.org/project/scheduled_updates

Scheduler: https://www.drupal.org/project/scheduler

# Themes

## Administration

Eleven: https://www.drupal.org/project/eleven

Material Admin: https://www.drupal.org/project/material_admin

## Base

AdaptiveTheme: https://www.drupal.org/project/adaptivetheme

Bear Skin: https://www.drupal.org/project/bear_skin

Bootstrap: https://www.drupal.org/project/bootstrap

Cog: https://www.drupal.org/project/cog

Zen: https://www.drupal.org/project/zen

ZURB Foundation: https://www.drupal.org/project/zurb_foundation

## Others

Advanced queue: https://www.drupal.org/project/advancedqueue

Configuration Form: https://www.drupal.org/project/config_form

Custom contextual links: https://www.drupal.org/project/ccl

Flag: https://www.drupal.org/project/flag

Rules: https://www.drupal.org/project/rules

Webform: https://www.drupal.org/project/webform

# Distributions

## Configurations

Configuration installer: https://www.drupal.org/project/config_installer

## Demo

Demo Framework: https://www.drupal.org/project/df

Drupal 8 multilingual demo: https://www.drupal.org/project/multilingual_demo

## Starter kit

Lightning: https://www.drupal.org/project/lightning

Panopoly: https://www.drupal.org/project/panopoly
